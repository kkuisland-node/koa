import mongoose from 'mongoose'
const {Schema} = mongoose

// Book 에서 사용할 서브다큐먼트의 스키마
const Author = new Schema({
	name: String,
	email: String,
})

const Book = new Schema({
	title: String,
	authors: [Author],
	publishedDate: Date,
	price: Number,
	tags: [String],
	createdAt: {
		// 기본값을 설정할땐 이렇게 객체로 설정
		type: Date,
		default: Date.now, // 긴본값은 현재 날짜로 지정합니다.
	},
})

/* 
   이 파일에선, 두개의 스키마를 만들었습니다. 하나는 저자의 정보가 들어가는 Author 스키마이고, 책의 정보가 들어가는 Book 스키마에서 authors 값의 형식을 지정할때 Author 스키마를 가진 객체들의 배열로 이뤄지도록 설정하였습니다.

   그리고, 하단에서는 mongoose.model 을 통하여 우리가 만든 스키마를 모델로 변환하고 다른 파일에서 불러와서 사용 할 수 있도록 내보내주었습니다.

   model 함수에선 기본적으론 두개의 파라미터를 필요로합니다. 첫번째는 파라미터는 해당 스키마의 이름이고, 두번째는 스키마 객체입니다. 스키마의 이름을 정해주면, 이의 복수형태로 컬렉션이름을 만들어줍니다.

   예를들어, Book 으로 설정한다면, 실제 데이터베이스에서 생성되는 컬렉션 이름은 books 입니다. 그리고, BookInfo 로 설정한다면 bookinfos 로 만들어집니다.

   MongoDB 에서 컬렉션 이름을 만들때의 컬렉션은 구분자를 사용하지 않고, (user_info 같은), 복수형태로 쓰는것 (books 처럼)인데요, 만약에 이 컨벤션을 따르고 싶지 않다면 세번째 파라미터로 여러분들이 원하는 이름을 정해주면 됩니다.

   예를들자면 다음과 같이 코드를 쓰면 되겠습니다.

   mongoose.model('Book', Book, 'custom_book_collection');
*/
export default mongoose.model('Book', Book, 'Books')
