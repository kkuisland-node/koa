module.exports = {
	env: {
		es2021: true,
		node: true,
	},
	extends: ['eslint:recommended', 'prettier'],
	parserOptions: {
		ecmaVersion: 12,
		sourceType: 'module',
	},
	rules: {
		// 'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'on',
		'no-debugger': 'error',
		// 'no-console': 'error',
	},
}
